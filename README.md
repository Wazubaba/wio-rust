Wazu IO library
=================================
This essentially replicates basic C `fopen()` functionality.

See the included rustdoc api(should be pretty concise
and approachable. If not, lemme know!) for usage.


Usage
=================================
Edit your Cargo.toml file, add this:
```
[dependencies.wio]
git = "git://wazu.info.tm/wio-rust.git"
```

Alternatively, if my server is acting up or you prefer
to stick to github:
```
[dependencies.wio]
git = "https://github.com/Wazubaba/wio-rust.git"
```


Now you can simply add `extern crate wio;` to your
code and it should be downloaded by cargo on your
next build. Don't forget to freshen it from
time-to-time with `cargo update`!


Dependencies
=================================
This was only really tested with:
`rustc 1.0.0-beta.2 (e9080ec39 2015-04-16) (built 2015-04-16)`

Probably ought to work with more recent versions provided
nothing in the main stdlib changes too much.


License
=================================
I didn't re-invent the wheel or anything here. MIT is
fine for this. If you need another license, lemme
know and we probably can work something out.

