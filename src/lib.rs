use std::io::prelude::*;
use std::path::Path;
use std::fs::OpenOptions;
use std::fs::File;
use std::io::Error;

/// Returns a `File` object with a given mode.
///
/// # Arguments
/// * `path`  - A valid `&str` pointing to a file.
/// * `flag`  - A `&str` that is either "a", "r", "w", or "rw".
/// "a"  - Append mode.
/// "r"  - Read-only mode.
/// "w"  - Write-only mode.
/// "rw" - Read+Write mode.
///
/// # Examples
/// ```
/// use std::fs::File;
///
/// fn main() {
///     let my_file = match wio::fopen("Cargo.lock", "r") {
///         Ok(file) => file,
///         Err(err) => panic!("Could not load file! ERR:{}", err),
///     };
/// }
/// ```
///
/// # Failures
/// This function will return the same errors that std::fs::File.open()
/// will return if there is a problem.
pub fn fopen(path: &str, flag: &str) -> Result<File, Error>
{
    let fpath = Path::new(path);
    let mut fp = OpenOptions::new();
    if flag == "a" {
        fp.create(true);
        fp.write(true);
        fp.append(true);
    } else
    if flag == "r" {
        fp.read(true);
    } else
    if flag == "w" {
        fp.truncate(true);
        fp.write(true);
        fp.create(true);
    } else
    if flag == "rw" {
        fp.read(true);
        fp.write(true);
        fp.create(true);
    }

    match fp.open(&fpath) {
        Ok(file) => Ok(file),
        Err(err) => Err(err),
    }
}

/// Returns a `File` object with a given mode.
///
/// # Arguments
/// * `path`  - A valid `Path` pointing to a file.
/// * `flag`  - A `&str` that is either "a", "r", "w", or "rw".
/// "a"  - Append mode.
/// "r"  - Read-only mode.
/// "w"  - Write-only mode.
/// "rw" - Read+Write mode.
///
/// # Examples
/// ```
/// use std::fs::File;
/// use std::path::Path;
///
/// fn main() {
///     let my_file = match wio::fopenp(Path::new("Cargo.lock"), "r") {
///         Ok(file) => file,
///         Err(err) => panic!("Could not load file! ERR:{}", err),
///     };
/// }
/// ```
///
/// # Failures
/// This function will return the same errors that std::fs::File.open()
/// will return if there is a problem.
pub fn fopenp(fpath: &Path, flag: &str) -> Result<File, Error>
{ // Open a file via a Path
    let mut fp = OpenOptions::new();
    if flag == "a" {
        fp.create(true);
        fp.write(true);
        fp.append(true);
    } else
    if flag == "r" {
        fp.read(true);
    } else
    if flag == "w" {
        fp.truncate(true);
        fp.write(true);
        fp.create(true);
    } else
    if flag == "rw" {
        fp.read(true);
        fp.write(true);
        fp.create(true);
    }

    match fp.open(&fpath) {
        Ok(file) => Ok(file),
        Err(err) => Err(err),
    }
}

#[test]
fn fopen_test()
{
    let _ = match fopen("Cargo.lock", "r") {
        Ok(file) => file,
        Err(err) => panic!("Unable to open file! ERR:{}", err),
    };
}

#[test]
fn fopenp_test()
{
    let _ = match fopenp(Path::new("Cargo.lock"), "r") {
        Ok(file) => file,
        Err(err) => panic!("Unable to open file! ERR:{}", err),
    };
}
